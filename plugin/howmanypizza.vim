" File: howmanypizza.vim
" Author: Ricardo Tun <github.com/ricrdo>
" Description: Calculates pizza to order
" Last Modified: June 20, 2015

if !exists('g:hmp_personSlices') | let g:hmp_personSlices = 3 | en
if !exists('g:hmp_pizzaSlices') | let g:hmp_pizzaSlices = 8 | en

fu! C2F(n)
  return a:n+0.0
endf

fu! HowManyPizza(people)
  let Pizzas = float2nr(ceil((C2F(g:hmp_personSlices)/C2F(g:hmp_pizzaSlices))*C2F(a:people)))
  echo "You'll need to order:" Pizzas "pizzas."
endf

fu! PizzaWizard()
  let g:hmp_personSlices = input("How many slices will eat each person? ", 3)
  let g:hmp_pizzaSlices = input("How many slices has a pizza? ", 8)
  let g:hmp_people = input("How many people are you? ", 4)
  echo "\n"
  call HowManyPizza(g:hmp_people)
endf

com! -n=? HMP call HowManyPizza(<q-args>)
